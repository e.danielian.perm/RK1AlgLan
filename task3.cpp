#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
using namespace std;

int main()
{
	int n = -1, m = -1, sh;
	string snm;
	getline(cin, snm);
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	stringstream ss;
	int k = snm.find(',');
	ss << snm.substr(0, k);
	ss >> n;
	ss.clear();
	snm.erase(0, k + 1);
	if (!snm.empty())
	{
		ss << snm;
		ss >> m;
		ss.clear();
	}
	if (n <= 0 || m <= 0)
	{
		cout << "An error has occured while reading input data"; system("pause");
		return 0;
	}
	int**a;
	a = new int*[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	//ввод матрицы
	int i = 0;
	do
	{
		string s;
		getline(cin, s);
		cin.clear();
		cin.ignore(cin.rdbuf()->in_avail());
		int k = s.find(' '), count = 0, x;
		if (k != -1)
		{
			ss << s.substr(0, k);
			ss >> x;
			ss.clear();
			s.erase(0, k + 1);
		}
		else
		{
			ss << s;
			ss >> x;
			ss.clear();
			s.erase(0);
		}
		if (s.empty())//в строке было рвоно 1 число => это было число сдвига
		{
			sh = x;
			break;
		}
		else
			a[i][count++] = x;
		k = s.find(' ');
		while (k != -1)
		{
			ss << s.substr(0, k);
			ss >> a[i][count++];
			ss.clear();
			s.erase(0, k + 1);
			k = s.find(' ');
		}
		if (!s.empty())
		{
			ss << s;
			ss >> a[i][count++];
			ss.clear();
		}
		i++;
	} while (true);
	if (i != n)
	{
		cout << "An error has occured while reading input data"; system("pause");
		return 0;
	}
	//сдвиг
	for (int kol = 0; kol < sh; kol++)
	{
		int t = a[0][0];
		for (int i = 0; i < n - 1; i++)
			a[i][0] = a[i + 1][0];
		for (int i = 0; i < m - 1; i++)
			a[n - 1][i] = a[n - 1][i + 1];
		for (int i = n - 1; i > 0; i--)
			a[i][m - 1] = a[i - 1][m - 1];
		for (int i = m - 1; i > 1; i--)
			a[0][i] = a[0][i - 1];
		a[0][1] = t;
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			cout <<setw(4) << a[i][j];
		cout << endl;
	}
	for (int i = 0; i < n; i++)
		delete[]a[i];
	delete[]a;
	return 0;
}