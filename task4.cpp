#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>
using namespace std;

int main()
{
	int n, m;
	string s;
	getline(cin, s);
	stringstream ss;
	int k = s.find(',');
	ss << s.substr(0, k);
	ss >> n;
	ss.clear();
	s.erase(0, k + 1);
	if (s.empty())
	{
		cout << "An error has occured while reading data";
		return 0;
	}
	ss << s;
	ss >> m;
	int **a;
	a = new int*[n];
	for (int i = 0; i < n; i++)
		a[i] = new int[m];
	int x = 1;
	int v = 0;//какой виток
	while (x <= n*m)
	{
		for (int i = v; i < m - v; i++)
			a[v][i] = x++;
		if (x > n*m) break;
		for (int i = 1 + v; i < n - v; i++)
			a[i][m - 1 - v] = x++;
		if (x > n*m) break;
		for (int i = m - 2 - v; i >= v; i--)
			a[n - 1 - v][i] = x++;
		for (int i = n - 2 - v; i >= 1 + v; i--)
			a[i][v] = x++;
		v++;
	}
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < m; j++)
			cout << setw(4) << a[i][j];
		cout << endl;
	}
	for (int i = 0; i < n; i++)
		delete[]a[i];
	delete[]a;
	return 0;
}
