#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main()
{
	int n;
	cin >> n;
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	string s;
	getline(cin, s);
	if (n <= 0)//неверное количество элементов
	{
		cout << "An error has occured while reading input data";
		return 0;
	}
	int *a;
	a = new int[n];
	int j = 0, k = s.find(' ');
	stringstream ss;
	while (k != -1)
	{
		ss << (s.substr(0, k));
		ss >> a[j++];
		ss.clear();
		s.erase(0, k + 1);
		k = s.find(' ');
	}
	if (!s.empty())
	{
		ss << (s.substr(0, k));
		ss >> a[j++];
	}
	if (j != n)//кол-во элементов не совпало с заданным
	{
		cout << "An error has occured while reading input data";
		return 0;
	}
	for (int i = 0; i < n / 2; i++)
		swap(a[i], a[n - i - 1]);
	for (int i = 0; i < n; i++)
		cout << a[i] << ' ';
	delete[]a;
	return 0;
}