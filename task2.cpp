#include <iostream>
#include <string>
#include <sstream>
using namespace std;

void reverse(int*a, int l, int r)
{
	while(l<r)
		swap(a[l++], a[r--]);
}

int main()
{
	int n, sh;
	cin >> n;
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	string s;
	getline(cin, s);
	cin.clear();
	cin.ignore(cin.rdbuf()->in_avail());
	string ssh;
	getline(cin, ssh);
	if (n <= 0)//неверное количество элементов
	{
		cout << "An error has occured while reading input data"; system("pause");
		return 0;
	}
	int *a;
	a = new int[n];
	int j = 0, k = s.find(' ');
	stringstream ss;
	while (k != -1)
	{
		ss << (s.substr(0, k));
		ss >> a[j++];
		ss.clear();
		s.erase(0, k + 1);
		k = s.find(' ');
	}
	if (!s.empty())
	{
		ss << (s.substr(0, k));
		ss >> a[j++];
	}
	if (j != n)//кол-во элементов не совпало с заданным
	{
		cout << "An error has occured while reading input data"; system("pause");
		return 0;
	}
	if (ssh.empty())
	{
		cout << "An error has occured while reading input data"; system("pause");
		return 0;
	}
	ss.clear();
	ss << ssh;
	ss >> sh;
	if (sh < 0)
	{
		cout << "An error has occured while reading input data"; system("pause");
		return 0;
	}
	//сдвиг
	reverse(a, 0, n-1);
	reverse(a, 0, sh-1);
	reverse(a, sh, n-1);
	//вывoд
	for (int i = 0; i < n; i++)
		cout << a[i] << ' ';
	delete[]a;
	return 0;
}